# My Dot Files

Kumpulan `dot files` berkas konfigurasi untuk:

1. Vim / Neovim
2. Tmux

Jika kalian ingin menggunakan berkas dalam repositori, harap diganti (rename) terlebih dahulu dengan menambahkan titik di awal nama berkas.

Contoh:
`vimrc` menjadi `.vimrc`

Dan pastikan pula kalian meletakan berkas tersebut di `$HOME`.

## Contoh Instalasi ViM

```
$ git clone git@gitlab.com:alisoftware/my-dot-files.git
$ cd my-dot-files/
$ cp vimrc ~/.vimrc
```
